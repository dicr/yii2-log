<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license proprietary
 * @version 05.01.22 20:55:41
 */

declare(strict_types = 1);
namespace dicr\log\manager;

use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ArrayDataProvider;

use function array_flip;
use function implode;
use function mb_stripos;
use function stripos;
use function strtotime;

use const SORT_ASC;
use const SORT_DESC;

/**
 * Фильтр сообщений.
 *
 * @property-read Message[] $messages
 * @property-read ArrayDataProvider $provider
 */
class MessageFilter extends Model
{
    public Log $log;

    public ?string $key = null;

    public ?string $dateFrom = null;

    public ?string $dateTo = null;

    public ?string $ip = null;

    public string|int|null $userId = null;

    public ?string $sessionId = null;

    public ?string $level = null;

    public ?string $category = null;

    public ?string $text = null;

    public ?string $lines = null;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        parent::init();

        if (! isset($this->log)) {
            throw new InvalidConfigException('log');
        }
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            ['key', 'trim'],
            ['key', 'default'],

            [['dateFrom', 'dateTo'], 'trim'],
            [['dateFrom', 'dateTo'], 'default'],
            [['dateFrom', 'dateTo'], 'date', 'format' => 'php:Y-m-d'],

            ['ip', 'trim'],
            ['ip', 'default'],

            ['userId', 'trim'],
            ['userId', 'default'],

            ['sessionId', 'trim'],
            ['sessionId', 'default'],

            ['level', 'trim'],
            ['level', 'default'],
            ['level', 'in', 'range' => Message::LEVELS],

            ['category', 'trim'],
            ['category', 'default'],

            ['text', 'trim'],
            ['text', 'default'],

            ['lines', 'trim'],
            ['lines', 'default'],
        ];
    }

    /**
     * Сравнивает сообщение с фильтром.
     */
    public function matchMessage(Message $message): bool
    {
        $levelsMap = array_flip(Message::LEVELS);

        return
            ($this->key === null || $message->key === $this->key) &&
            ($this->userId === null || $message->userId === $this->userId) &&
            ($this->sessionId === null || stripos($message->sessionId, $this->sessionId) !== false) &&
            ($this->category === null || stripos($message->category, $this->category) === 0) &&
            ($this->text === null || mb_stripos($message->text, $this->text) !== false) &&
            ($this->level === null || $levelsMap[$message->level] <= $levelsMap[$this->level]) &&
            ($this->ip === null || stripos($message->ip, $this->ip) !== false) &&
            ($this->dateFrom === null || strtotime($message->date) >= strtotime($this->dateFrom)) &&
            ($this->dateTo === null || strtotime($message->date) <= strtotime($this->dateTo)) &&
            ($this->lines === null || mb_stripos(implode($message->lines), $this->lines) !== false);
    }

    /**
     * Сообщения.
     *
     * @return Message[]
     * @throws Exception
     */
    public function getMessages(): array
    {
        return $this->validate() ? $this->log->parse([$this, 'matchMessage']) : [];
    }

    private ArrayDataProvider $_provider;

    /**
     * Провайдер данных.
     */
    public function getProvider(): ArrayDataProvider
    {
        if (! isset($this->_provider)) {
            $this->_provider = new ArrayDataProvider([
                'key' => 'key',
                'allModels' => $this->messages,
                'sort' => [
                    'attributes' => [
                        'date' => [
                            'asc' => ['date' => SORT_ASC],
                            'desc' => ['date' => SORT_DESC],
                            'default' => SORT_DESC
                        ],
                        'ip', 'userId', 'sessionId', 'level', 'category', 'text'
                    ],
                    'defaultOrder' => [
                        'date' => SORT_DESC
                    ]
                ],
                'pagination' => [
                    'pageSizeLimit' => [1, 100],
                    'defaultPageSize' => 100
                ]
            ]);
        }

        return $this->_provider;
    }
}
