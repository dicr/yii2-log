<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license proprietary
 * @version 05.01.22 20:58:10
 */

declare(strict_types = 1);
namespace dicr\log\manager;

use yii\base\InvalidConfigException;
use yii\base\Model;

use function md5;
use function preg_match;

/**
 * Элемент лога.
 *
 * @property-read Log $log
 */
class Message extends Model
{
    public const LEVELS = [
        'error', 'warning', 'info', 'trace', 'profile'
    ];

    public Log $log;

    /** идентификатор сообщения */
    public string $key;

    public string $date;

    public ?string $ip = null;

    public ?string $userId = null;

    public ?string $sessionId = null;

    public ?string $level = null;

    public ?string $category = null;

    /** текст сообщения */
    public ?string $text = null;

    /** @var string[] остальные строки сообщения */
    public array $lines = [];

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        parent::init();

        if (! isset($this->log)) {
            throw new InvalidConfigException('log');
        }

        if (empty($this->key)) {
            throw new InvalidConfigException('key');
        }

        if (empty($this->date)) {
            throw new InvalidConfigException('date');
        }
    }

    /**
     * Парсит строку лога.
     *
     * @param string $line строка начала сообщения
     * @return array|null конфиг Message если распознано начало лога или null
     */
    public static function parseLine(string $line): ?array
    {
        $matches = null;

        return preg_match(
            '~^(\d+\-\d+\-\d+\s+\d+\:\d+\:\d+)\s+\[([^\]]+)\]\[([^\]]+)\]\[([^\]]+)\]\[(\S+)\]\[([^\]]+)\]\s+(.+)$~u',
            $line, $matches
        ) ? [
            'key' => md5($line),
            'date' => $matches[1],
            'ip' => $matches[2],
            'userId' => $matches[3] === '-' ? null : $matches[3],
            'sessionId' => $matches[4] === '-' ? null : $matches[4],
            'level' => $matches[5],
            'category' => $matches[6],
            'text' => $matches[7],
            'lines' => []
        ] : null;
    }
}
